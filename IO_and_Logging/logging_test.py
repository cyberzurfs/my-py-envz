import os
import logging
import glob
from datetime import datetime
from dotenv import load_dotenv

if __name__ == "__main__":

    load_dotenv()

    log_path = os.getenv('LOG_PATH')
    log_level = os.getenv('LOG_LEVEL')
    logger = logging.getLogger()
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', 
                        filename=log_path,
                        level=log_level)
    logger.setLevel(log_level)

    var = "Starting Cyberzurfs Development Course"
    logger.info(f'{var}')
    print((f'{var}'))
