import os
import sys
from dotenv import load_dotenv

def main():

    load_dotenv()
    try:

        # Example 1 - Hard coded directory/file...
        #file = '/home/cyberzurfs/cyberzurfspace/repo/pythoning_proyects/file_with_content.txt' # Replace ubuntu with your User... 

        # This file will wait for a system argument (in this example a file...)
        # Example 2 - Parametrizar (pass arguments)

        file = sys.argv[1]

    except Exception as ex:
        print (f"Encountered Error {ex} Reading from file, Check system argument!")
        file = os.getenv('FILE_WITH_CONTENT_PATH')

    with open (file, 'r+') as f:
        content = f.read()
        print (f'File Content: {content}')


if __name__ == "__main__":
    sys.exit(main())